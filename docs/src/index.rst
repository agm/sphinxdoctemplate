Welcome to My Project
=====================
Whatever you write here will show up after the contents and as a prelude to the chapters. 

-----

**Contents**:

.. toctree::
   :maxdepth: 2

   chapter1
   chapter2



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`search`



Admin and Project Maintainer
============================
Ashray Manur
ashraymanur@gmail.com