---
Contains the Sphinx documentation template. This will be used to generate PDFs and web-based documentation. 

## How to use this?

* Download the `docs` folder. 
* The ``docs/src`` contains the source files. 
* Change the project variables in  `docs/src/conf.py`
* To generate HTML documentation : `make html` (in the `docs` folder). The generated HTML will be located in the location specified by `conf.py`, in this case `doc/_build/html`.
* To generate PDF documentation: `make latexpdf` (in the `docs` folder). The generated LateX files and PDF will be located in the location specified by `conf.py`, in this case `_build/latex`. Copy the `.pdf` files to the desired location. *Make sure you have a LaTeX distribution installed on your machine*. 


## Sphinx and reStrucuturedText Resources
* [Sphinx website](http://www.sphinx-doc.org)
* [Projects using Sphinx](http://www.sphinx-doc.org/en/master/examples.html)
* [Tutorial for custom docs](https://matplotlib.org/sampledoc/)
* [reStructuredText cheatsheet](https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst)

## Things to changee in `conf.py`

### General info
```
# General information about the project.
project = u'MyProject'
copyright = u'2018, MyProject'
author = u'Ashray Manur'
```

### Version

```
The short X.Y version.
version = u'0.1.0'
# The full version, including alpha/beta/rc tags.
release = u'0.1.0'
```

### O/p file base name

```
# Output file base name for HTML help builder.
htmlhelp_basename = 'MyProject'
```

### Info for LaTeX documentation

```
latex_documents = [
    (master_doc, 'MyProject.tex', u'My Project (full title)',
     u'Ashray Manur', 'manual'),
]
```

### Parameters for manual page 

```

# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'MyProject', u'My Project (full title)',
     [author], 1)
]
```

### If you want to change the theme (optional)
```
# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinxdoc'
```

## If you want to add images/media
Create a folder in `docs/src/_static`

## Current Version

1.0.0

## LICENSE

IT can be found [here](LICENSE.md)

## CONTRIBUTING GUIDE

It can be found [here](CONTRIBUTING.md)

## CHANGELOG

It can be found [here](CHANGELOG.md)

## BUGS 

It can be found [here](BUGS.md)


## Forking for own use?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.
