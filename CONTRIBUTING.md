# Contributing to this Document 
Thanks for your interest in contributing to the Sphinx Documentation Template. This guide details how to contribute to this document. 

**There are different ways to contribute**

1. If you find a bug, please raise an issue [here](https://gitlab.com/agm/sphinxdoctemplate/issues) first. Then create a merge request [here](https://gitlab.com/seup/sphinxdoctemplate/merge_requests). 
2. You can also check the issue section to find issues with a tag `Accepting merge requests`. These are usually very specific problems you can work on. 


